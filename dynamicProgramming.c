#include "list.h"

//Dynamic Programming Algorithm
//O(n) + O(n) = O(n) ->Time Complexity
//Space Complexity ->O(n)
int MaxSubarray4(Node *h){

    if(h==NULL){
        printf("List is not found.");
        return INT_MIN;
    }
    Node *max_end_here, *temp;
    max_end_here = h;
    temp = h->next;
    Node *a;

    while (temp!= NULL && max_end_here!=NULL) {
        if ((temp->number + max_end_here->number) > 0) {
            max_end_here->next->number= temp->number+max_end_here->number;

        } else {
            max_end_here->next->number = temp->number;
        }
        temp=temp->next;

        //printf("array-value add: %d\n",max_end_here->next->number);
        max_end_here=max_end_here->next;
    }
    int max=0;
    a=max_end_here;
    while (a != NULL) {
        if((a->number)>max){
            max= a->number;
            //printf("xx-%d\n",a->number);
        }

        a=a->next;
    }
    return  max;
}
