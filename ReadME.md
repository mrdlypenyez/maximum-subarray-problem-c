# Maximum Sum Subarray Problem C
In computer science, the maximum sum subarray problem is the task of finding a contiguous subarray with the largest sum,within a given one-dimensional array A[1...n] of numbers.

## Problem Description: 
You are given an array A[] with n elements. You need to find the maximum sum of a subarray among all subarrays of that array. A subarray of array A[] of length n is a contiguous segment from A[i] through A[j] where 0<= i <= j <= n. Some properties of this problem are:

- If the array contains all non-negative numbers, the maximum subarray is the entire array.
- Several different sub-arrays may have the same maximum sum.

## Example
- Input: [2,-1,5,5]
- Output: 11 the subarray all value the max sum
  | 2  | -1  |  5  | 5  |
  |----|-----|-----|----|

## Comparison of Solutions
This problem can be solved using several different algorithmic techniques, including brute force,divide and conquer,dynamic programming.Kadane and reduction to shortest paths.
| Solution Approach               | Time Complexity | Space Complexity |
|---------------------------------|-----------------|------------------|
| Brute Force 1                   |  O(n^3)         |  O(1)            |
| Brute Force 2                   | O(n^2)          | O(1)             |
| Divide and Conquer              | O(nlogn)        | O(logn)          |
| Dynamic Programming -temp array | O(n)            | O(n)             |
| Kadane Algorithm                | O(n)            | O(1)             |