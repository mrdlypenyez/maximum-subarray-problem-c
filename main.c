#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "divideConquer.c"
#include "dynamicProgramming.c"
#include "bruteForce1.c"
#include "bruteForce2.c"
#include "KadaneAlgorithm.c"
int main()
{
    int i,n=0,tempNumber;
    head=(Node *)malloc(sizeof (Node));
    tail=(Node *)malloc(sizeof (Node));
    head->number=2;
    head->next=tail;
    tail->number=-1;
    tail->next=NULL;
    testAdd();
    testAdd();

    //array ->>> 2, -1 , 5, 5  ->Subarray=11

    printf("\n Brute Force1 O(n^3) -> max subarray sum: %d\n",MaxSubarray1(head));

    printf(" Brute Force2 O(n^2) -> max subarray sum: %d\n", MaxSubarray2(head));

    printf(" Divide and Conquer O(nlogn) -> max subarray sum: %d\n", MaxSubarray3(head,0, NodeCount(head)));

    printf(" Dynamic Programming ->O(n)+O(n)=O(n) -> max subarray sum: %d\n", MaxSubarray4(head));

    printf("Kadane Algorithm ->O(n)->max subarray sum: %d", KadaneMaxSubarraySum(head));

    return 0;
}




