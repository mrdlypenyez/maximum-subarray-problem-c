//
// Created by Zeynep on 19.04.2021.
//

#ifndef MAXIMUM_SUBARRAY_PROBLEM_C_LIST_H
#define MAXIMUM_SUBARRAY_PROBLEM_C_LIST_H
#include <stdio.h>
#include <stdlib.h>
typedef struct List{
    int number;
    struct List *next;
}Node;

Node *head,*tail;

void testAdd();
void Add(int number);
int NodeCount(Node *head);
int max(int a,int b,int c);



#endif //MAXIMUM_SUBARRAY_PROBLEM_C_LIST_H
