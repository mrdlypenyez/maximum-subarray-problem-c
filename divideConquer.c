#include "list.h"

//Divide and Conquer similar to merge sort
//Time Complexity-> T(n) = 2T(n/2) + O(n) = O(nlogn)
//Space Complexity = O(logn), stack space by recursive calls

int maxCrossingSum(Node *a,int l,int mid,int r);
int MaxSubarray3(Node *temp,int low,int high){
    if(low==high) return 0;
    else{
        int mid=low + (high - low)/2;
        int left_sum = MaxSubarray3(temp, low, mid);
        int right_sum = MaxSubarray3(temp, mid+1, high);
        int crossing_Sum = maxCrossingSum(temp, low, mid, high);

        return max (left_sum, right_sum, crossing_Sum);
        // printf("%d,%d",left_sum,right_sum);


    }
}
int maxCrossingSum(Node *a,int l,int mid,int r){
    int sum=0,i;
    Node *temp;
    temp=a;
    int lsum = INT_MIN;
    for(i=mid;i<r+1;i++){
        sum+=temp->number;
        if(sum>lsum) lsum=sum;
        temp=temp->next;
    }
    sum=0;
    int rsum=INT_MIN;
    for(i=mid+1;i<r;i++){
        sum+=temp->number;
        //printf("\n%d\n",temp->number);
        if(sum > rsum) rsum = sum;
    }
    return (lsum+rsum);
}
