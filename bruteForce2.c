#include "list.h"

//Brute Force -> O(n^2) complexity -> Space complexity ->O(1)
int MaxSubarray2(Node *head){
    int i,j,max_sum=0,sum;
    Node *temp1,*temp2;
    temp1=head;
    if(temp1==NULL){
        printf("List is not found.");
        return -1;
    }

    while (temp1!=NULL){
        sum=0;
        temp2=temp1;
        while (temp2!=NULL){
            sum+=temp2->number;
            if(sum>max_sum){
                max_sum=sum;
            }
            temp2=temp2->next;
        }
        temp1=temp1->next;
    }
    return max_sum;
}

