#include "list.h"

//Brute Force 1 ->O(n^3) complexity
int MaxSubarray1(Node *x){
    int max_sum=0,i,j,k,sum=0;
    Node *temp1,*temp2,*temp3;


    temp1=x;
    if(temp1==NULL){
        printf("List is not found.");
        return -1;
    }
    temp3=temp1;
    temp2=temp1;
    while(temp1!=NULL){
        printf(" %d ",temp1->number);
        while (temp2!=NULL){
            while (temp3!=NULL){
                sum=sum+temp3->number;
                //printf("add number:%d   sum: %d ",temp3->number,sum);
                temp3=temp3->next;
            }
            if(sum>max_sum){
                max_sum=sum;
            }
            temp2=temp2->next;
        }

        temp1=temp1->next;
    }
    return max_sum;
}

