#include "list.h"
#include "stdlib.h"
#define MAX(a,b) (((a)>(b))?(a):(b))
int KadaneMaxSubarraySum(Node *head){
    int max_so_far=0;
    int max_ending_here = 0;
    Node  *temp;
    temp=head;
    while (temp!=NULL){

        //printf(" number: %d\n",temp->number);
        max_ending_here=temp->number; //2 - 1  5  5
        //printf("max_end:%d number: %d\n",max_ending_here,temp->number);
        if(max_ending_here<0)
            max_ending_here=0;
        max_so_far= MAX(temp->number,max_so_far);
        temp=temp->next;
    }
    return max_so_far;
}

